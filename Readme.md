# SnakePong

## Information
SnakePong assignment for my algorithms course.

## What it has to do:
Implement the game SnakePong (or PongSnake, Pong meets Snake, Snake vs. Pong, etc?)
Example https://twitter.com/wombatstuff/status/963777974570045440
Game consists of a maze, a snake, a paddle and a ball
Simple maze: just a rectangular wall, with one side opened
At start: the snake and paddle consist of 5 body parts, ball is placed in between and is sent directed towards the snake
The player controls the snake in four directions, up, down, left ,right. A single step at a time.
Every step check if the snake hits the wall or itself => game over
Every step check if the ball is in the open wall zone => snake gains a body part and ball restarts in the opening position
Every step check if the ball hits the wall, paddle or snake => the ball direction changes perpendicular to its current direction
Build your own data structure classes (only use primitive data types and arrays)
No predefined classes are allowed, except for a test framework, e.g. for Java JUnit the Math class.

## What it does:
Everything except for ball and snake collision.

![Screenshot](pic/screenshot.PNG)

## Sources
- Emiel Estievenart
- https://www.ntu.edu.sg/home/ehchua/programming/java/J8a_GameIntro-BouncingBalls.html
- http://www.java2novice.com/data-structures-in-java/linked-list/doubly-linked-list/
- http://www.dreamincode.net/forums/topic/328708-simple-java-pong-game-netbeans/
