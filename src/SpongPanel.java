import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class SpongPanel extends JPanel implements KeyListener{
	
	private Pong pong;
	private Snake snake;
	private Ball ball;
	private Wall wall;
	private Timer tickTimer;
	private static int snakeScore = 0;
	private static int pongScore = 0;
	private int tickCount = 0;
	
	public void initAll() {
		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				tick();
			}
		};
		this.pong = new Pong(40, 225, 10, 160, Color.blue);
		this.snake = new Snake(6, 20);
		this.ball = new Ball(100,50 ,20, Color.red); 
		this.wall = new Wall(0, 0, 800, 600, 20, Color.black);
		
		snake.initSnake();
		tickTimer = new Timer();
		tickTimer.scheduleAtFixedRate(task, 0, 20);
		
	}
	
	public static void main(String[] args) {
		JFrame frm = new JFrame("Snake Pong v1.0");
		SpongPanel pnl = new SpongPanel();
		pnl.setPreferredSize(new Dimension(800, 600));
	    frm.add(pnl, BorderLayout.CENTER);
	    frm.setLocation(150, 100);
	    frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frm.setResizable(false);
	    frm.pack();
	    frm.addKeyListener(pnl);
	    pnl.initAll();
	    frm.setVisible(true);
	}
	
	@Override
	public void paint(Graphics g) {
		g.clearRect(0, 0, getWidth(), getHeight()); //Clear screen before draw operations
		pong.drawPong(g);
		snake.drawSnake(g);
		ball.drawBall(g);
		wall.drawWalls(g);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 16)); 
		g.drawString("Score: " + snakeScore, 580,35);
		g.drawString("Score: " + pongScore, 130,35);
		if(snake.getDirection() == Snake.Direction.DONE) {
			g.setFont(new Font("TimesRoman", Font.PLAIN, 28)); 
			g.drawString("GAME OVER!", 315, 300);
		}
	}
	
	public void tick() {
		tickCount++;
		repaint();
		if(tickCount % 6 == 0) {
			snake.moveSnake();
			snake.detectWallCollision(wall, ball, pong);
			snake.detectCollisionWithSelf();
		}	
		ball.detectSnakeCollision(snake);
		ball.detectPongCollision(pong);
		ball.detectWallCollision(wall, snake);
		pong.detectWallCollision(wall);
		if(snake.getDirection() == Snake.Direction.DONE) {
			gameOver();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent arg0) {
		switch(arg0.getKeyCode()) {
			case KeyEvent.VK_UP:
			{
				if(snake.getDirection() != Snake.Direction.DOWN && snake.getDirection() != Snake.Direction.DONE) {
					snake.setDirection(Snake.Direction.UP);
				}
				break;
			}
			case KeyEvent.VK_DOWN: 
			{
				if(snake.getDirection() != Snake.Direction.UP && snake.getDirection() != Snake.Direction.DONE) {
					snake.setDirection(Snake.Direction.DOWN);
				}
				break;
			}
			case KeyEvent.VK_LEFT:
			{
				if(snake.getDirection() != Snake.Direction.RIGHT && snake.getDirection() != Snake.Direction.DONE) {
					snake.setDirection(Snake.Direction.LEFT);
				}
				break;
			}
			case KeyEvent.VK_RIGHT:
			{
				if(snake.getDirection() != Snake.Direction.LEFT && snake.getDirection() != Snake.Direction.DONE) {
					snake.setDirection(Snake.Direction.RIGHT);
				}
				break;
			}
			case KeyEvent.VK_Z:
			{
				pong.setDirection(Pong.Direction.UP);
				break;
			}
			case KeyEvent.VK_S:
			{
				pong.setDirection(Pong.Direction.DOWN);
				break;
			}
			
		}
	}
	
	public void gameOver() {
		ball.setXSpeed(0);
		ball.setYSpeed(0);
		pong.setDirection(Pong.Direction.STILL);
		snake.setDirection(Snake.Direction.DONE);
	}
	
	public static void updateSnakeScore() {
		snakeScore++;
	}
	
	public static void updatePongScore() {
		pongScore++;
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}
	
}
