import java.awt.Color;
import java.awt.Graphics;

public class SnakePart {
	
	private int width, height, x, y;
	private Color color;
	
	public SnakePart(int x, int y, int width, int height, Color color) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.color = color;
	}
	
	public void drawSnakePart(Graphics g) {
		g.setColor(color);
		g.fillOval(x, y, width, height);
	}
	
	public int getX() {
		return this.x;
	}
	
	public void setX(int value) {
		x = value;
	}
	
	public int getY() {
		return this.y;
	}
	
	public void setY(int value) {
		y = value;
	}
	
}
