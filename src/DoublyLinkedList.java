import java.util.Iterator;
import java.util.NoSuchElementException;

public class DoublyLinkedList<E> implements Iterable<E> {
	 
    private Node head;
    private Node tail;
    private int size;
     
    public DoublyLinkedList() {
        size = 0;
        head = null;
        tail = null;
    }
    
    public DoublyLinkedList(E element) {
    	size = 1;
    	head = new Node(element);
    	tail = head;
    }

    public int size() { return size; }

    public boolean isEmpty() { return size == 0; }

    public void addFirst(E element) {
        Node tmp = new Node(element, head, null);
        if(head != null ) {head.prev = tmp;}
        head = tmp;
        if(tail == null) { tail = tmp;}
        size++;
    }
     
    public void addLast(E element) {
         
        Node tmp = new Node(element, null, tail);
        if(tail != null) {tail.next = tmp;}
        tail = tmp;
        if(head == null) { head = tmp;}
        size++;
    }
     
    public E removeFirst() {
        if (size == 0) throw new NoSuchElementException();
        Node tmp = head;
        head = head.next;
        head.prev = null;
        size--;
        return tmp.element;
    }
     
    public E removeLast() {
        if (size == 0) throw new NoSuchElementException();
        Node tmp = tail;
        tail = tail.prev;
        tail.next = null;
        size--;
        return tmp.element;
    }
    
    public E getHead() {
    	return head.element;
    }
    
    public E getTail() {
    	return tail.element;
    }
    
    public E getElementAt(int index) {
    	if(index+1 > size) {
    		return null;
    	}
    	if(index == 0) {
    		return head.element;
    	}
    	Node temp = head;
    	for(int i = 0; i < index; i++) {
    		temp = temp.next;
    	}
    	return temp.element;
    }
    
    private class Node {
        E element;
        Node next;
        Node prev;
        
        public Node(E element) {
			this(element, null, null);
		}
 
        public Node(E element, Node next, Node prev) {
            this.element = element;
            this.next = next;
            this.prev = prev;
        }

		public Node getNext() {
			return next;
		}

		public E getElement() {
			return element;
		}
    }

    @Override 
	public Iterator<E> iterator() {
		return new LinkedListIterator();
	}
	
	private class LinkedListIterator implements Iterator<E>{
		
		private Node cursor;
		
		 public LinkedListIterator()
	      {
	         cursor = head;
	      }
	
		public boolean hasNext() {
			  return cursor != null;
		}

		public E next() { 
			E element = cursor.getElement();
			cursor = cursor.getNext();
			return element;
		}

	}

}
