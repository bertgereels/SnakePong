import java.awt.Color;
import java.awt.Graphics;

public class Ball {
	
	private int radius, x, y;
	private int initX, initY, initXSpeed, initYSpeed;
	private int xSpeed, ySpeed;
	private Color color;
	
	public Ball(int x, int y, int radius, Color color) {
		this.x = x;
		this.y = y;
		initX = x;
		initY = y;
		this.radius = radius;
		this.color = color;
		this.xSpeed = 4;
		initXSpeed = xSpeed;
		this.ySpeed = 4;
		initYSpeed = ySpeed;
	}
	
	public void drawBall(Graphics g) {
		g.setColor(color);
		g.fillOval(x, y, radius,radius);
	}
	
	public void initBall() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		x = initX;
		y = initY;
		xSpeed = initXSpeed;
		ySpeed = initYSpeed;
	}

	public void moveBall() {
		x += xSpeed;
		y += ySpeed;	
	}
	
	public void detectPongCollision(Pong pong) {
		if(x <= pong.getX() + pong.getWidth() && y >= pong.getY() && y <= pong.getY() + pong.getHeight()) {
			x = pong.getX() + pong.getWidth();
			xSpeed = - xSpeed;
		}
	}
	
	public void detectWallCollision(Wall wall, Snake snake) {
		int ballMinX = wall.getMinX() + wall.getWallThickness();
	    int ballMinY = wall.getMinY() + wall.getWallThickness();
	    int ballMaxX = wall.getMaxX() - radius - wall.getWallThickness();
	    int ballMaxY = wall.getMaxY() - radius - wall.getWallThickness();
	    
	    x += xSpeed;
		y += ySpeed;
		
	    if (x < ballMinX) {
	    	snake.growSnake();
		    SpongPanel.updateSnakeScore();
	    	initBall();
	    } else if (x > ballMaxX) {
	    	xSpeed = -xSpeed;
	        x = ballMaxX;
	        SpongPanel.updatePongScore();
	    }
	    if (y < ballMinY) {
	    	ySpeed = -ySpeed;
	        y = ballMinY;
	    } else if (y > ballMaxY) {
	    	ySpeed = -ySpeed;
	        y = ballMaxY;
	    }
	}
	
	public void detectSnakeCollision(Snake snake) {
		DoublyLinkedList<SnakePart> tempSnake = snake.snake;
		for (SnakePart part : tempSnake)
		{
			if(snake.getDirection() == Snake.Direction.UP || snake.getDirection() == Snake.Direction.DOWN) {
				if(x == part.getX() && y == part.getY()) {
					xSpeed = - xSpeed;
					ySpeed = - ySpeed;
					x = part.getX();
					System.out.println("Collision");
				}
			}
			
		}
	}
	
	public void setXSpeed(int value) {
		xSpeed = value;
	}
	
	public void setYSpeed(int value) {
		ySpeed = value;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
}
