import java.awt.Color;
import java.awt.Graphics;

public class Snake {
	
	DoublyLinkedList<SnakePart> snake;
	private int snakeLength, partSize, initSnakeX, initSnakeY;
	public enum Direction {UP, DOWN, LEFT, RIGHT, STILL, DONE};
	private Direction dir = Direction.UP;
			
	public Snake(int length, int partSize) {
		snake = new DoublyLinkedList<SnakePart>();
		this.snakeLength = length;
		this.partSize = partSize;
		this.initSnakeX = 600;
		this.initSnakeY = 300;
	}
	
	public void drawSnake(Graphics g) {
		for (SnakePart part : snake)
		{
			part.drawSnakePart(g);
		}
	}
	
	public void initSnake() {
		SnakePart headPart =  new SnakePart(initSnakeX,initSnakeY,partSize,partSize, Color.green);
		snake.addLast(headPart);
		for(int i = 1; i <= this.snakeLength; i++) {
			snake.addLast(new SnakePart(initSnakeX,initSnakeY+i*partSize,partSize,partSize,Color.black)); 
		}
	}
	
	public void growSnake() {
		snake.addLast(new SnakePart(-100,-100,partSize,partSize,Color.black));
		snakeLength++;
	}
	
	public void setDirection(Direction dir) {
		this.dir = dir;
	}
	
	public Direction getDirection() {
		return dir;
	}
	
	public void moveSnake() {
		switch (dir){
			case UP:
			{
				snake.addFirst(snake.removeLast());
				SnakePart newHead = snake.getHead();
				SnakePart previousHead = snake.getElementAt(1);
				newHead.setX(previousHead.getX());
				newHead.setY(previousHead.getY() - partSize);
				break;
			}
			case DOWN:
			{
				snake.addFirst(snake.removeLast());
				SnakePart newHead = snake.getHead();
				SnakePart previousHead = snake.getElementAt(1);
				newHead.setX(previousHead.getX());
				newHead.setY(previousHead.getY() + partSize);
				break;
			}
			case LEFT:
			{
				snake.addFirst(snake.removeLast());
				SnakePart newHead = snake.getHead();
				SnakePart previousHead = snake.getElementAt(1);
				newHead.setX(previousHead.getX()  - partSize);
				newHead.setY(previousHead.getY());
				break;
			}
			case RIGHT:
			{
				snake.addFirst(snake.removeLast());
				SnakePart newHead = snake.getHead();
				SnakePart previousHead = snake.getElementAt(1);
				newHead.setX(previousHead.getX()  + partSize);
				newHead.setY(previousHead.getY());
				break;
			}
			case STILL:
			{
				break;
			}
		}
	}
	
	public void detectWallCollision(Wall wall, Ball ball, Pong pong) {
		SnakePart currentHead  = snake.getHead();
		int snakeMinX = wall.getMinX() + wall.getWallThickness();
		int snakeMinY = wall.getMinY() + wall.getWallThickness();
		int snakeMaxX = wall.getMaxX() - partSize - wall.getWallThickness();
		int snakeMaxY = wall.getMaxY() - partSize - wall.getWallThickness();
		
		//Game over if one of these is true;
		if (currentHead.getX() < snakeMinX) {
			setDirection(Direction.DONE);
		} else if (currentHead.getX() > snakeMaxX) {
			setDirection(Direction.DONE);
		}
		
		if (currentHead.getY() < snakeMinY) {
			setDirection(Direction.DONE);
		} else if (currentHead.getY() > snakeMaxY) {
			setDirection(Direction.DONE);
		} 
	}
	
	public void detectCollisionWithSelf() {
		SnakePart currentHead = snake.getHead();
		
		for(int i = 1; i <= snakeLength; i++) {
			if(currentHead.getX() == snake.getElementAt(i).getX() && currentHead.getY() == snake.getElementAt(i).getY()) {
				setDirection(Direction.DONE);
			}
		}
	}
	
}
